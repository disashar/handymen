# Handymen

## To run in cmd:

### Client-side:

    cd projy
    npm start

### Server-side:

    cd backend
    nodemon app

### Demo login:

    biz@gmail.com
    -------------------
    customer@gmail.com

    password: 123123

## Endpoints schema

![endpoints path](endpoints.png "endpoints path")
