const express = require("express");
const app = express();
const http = require("http").Server(app);
const mongoose = require("mongoose");
const cors = require("cors");

require("dotenv").config({ path: "./config.env" });

app.use(cors());
app.use(express.json({ limit: "50mb" }));

mongoose
  .connect(process.env.DB, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  })
  .then(() => {
    console.log("mongodb connected successfully!");
  })
  .catch((err) => {
    console.error("mongodb failed to connect.", err);
  });

const users = require("./routes/users");
const bizs = require("./routes/bizs");

app.use("/users", users);
app.use("/bizs", bizs);

const port = process.env.PORT || 8000;
http.listen(port, () => console.log(`Listening on port ${port}...`));
