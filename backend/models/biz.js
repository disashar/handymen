const Joi = require("@hapi/joi");
const mongoose = require("mongoose");

const bizSchema = new mongoose.Schema({
  profession: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 255,
  },
  address: {
    type: String,
    required: false,
    maxlength: 255,
  },
  bizName: {
    type: String,
    required: false,
    maxlength: 255,
  },
  description: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 2048,
  },
  experience: {
    type: String,
    required: true,
  },
  email: {
    type: String,
  },
  name: {
    type: String,
  },
  image: {
    type: String,
    required: true,
  },
  phone: {
    type: String,
    required: true,
    minlength: 9,
    maxlength: 10,
  },
});

const Biz = mongoose.model("Biz", bizSchema);

function validateBiz(biz) {
  const schema = Joi.object({
    name: Joi.string().min(2).max(255).required(),
    email: Joi.string().min(6).max(255).required().email(),
    password: Joi.string().min(6).max(1024).required(),
    profession: Joi.string().min(2).max(255).required(),
    address: Joi.string().max(255).allow(""),
    bizName: Joi.string().max(255).allow(""),
    description: Joi.string().min(2).max(2048).required(),
    experience: Joi.string().required(),
    biz: Joi.boolean(),
    phone: Joi.string()
      .min(9)
      .max(10)
      .required()
      .regex(/^0[2-9]\d{7,8}$/),
    image: Joi.string(),
  });
  return schema.validate(biz);
}
function validateBizEdit(biz) {
  const schema = Joi.object({
    _id: Joi.string(),
    profession: Joi.string().min(2).max(255).required(),
    address: Joi.string().max(255).allow(""),
    bizName: Joi.string().max(255).allow(""),
    description: Joi.string().min(2).max(2048).required(),
    experience: Joi.string().required(),
    phone: Joi.string()
      .min(9)
      .max(10)
      .required()
      .regex(/^0[2-9]\d{7,8}$/),
    image: Joi.string().min(11),
  });
  return schema.validate(biz);
}

exports.Biz = Biz;
exports.validateBiz = validateBiz;
exports.validateBizEdit = validateBizEdit;
