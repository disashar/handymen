const express = require("express");
const router = express.Router();
const { User } = require("../models/user");
const { Biz, validateBiz, validateBizEdit } = require("../models/biz");
const tokenAuth = require("../middlewares/tokenAuth");
const _ = require("lodash");
const bcrypt = require("bcrypt");

router.post("/bizsignup", async (req, res) => {
  const { error } = validateBiz(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let user = await User.findOne({ email: req.body.email });
  if (user) return res.status(400).send("Email allready registerd.");

  let biz = new Biz(
    _.pick(req.body, [
      "profession",
      "address",
      "bizName",
      "description",
      "experience",
      "email",
      "name",
      "phone",
      "image",
    ])
  );
  if (!biz.image)
    biz.image =
      "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png";

  await biz.save();

  user = new User(_.pick(req.body, ["name", "email", "password", "biz"]));
  const salt = await bcrypt.genSalt(10);
  user.password = await bcrypt.hash(user.password, salt);

  await user.save();

  res.send(_.pick(user, ["_id", "name", "email", "biz"]));
});

router.get("/", async (req, res) => {
  const cards = await Biz.find({});
  res.send(cards);
});

router.get("/getcardbyuserid", tokenAuth, async (req, res) => {
  try {
    let card = await Biz.find({ email: req.user.email });
    res.send(card);
  } catch (err) {
    res.send("sorry not found");
  }
});

router.get("/getcardbycardid", async (req, res) => {
  try {
    let card = await Biz.findOne({ _id: req.query.id });
    res.send(card);
  } catch (err) {
    res.send(err);
  }
});

router.put("/", tokenAuth, async (req, res) => {
  const { error } = validateBizEdit(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let biz = await Biz.findOneAndUpdate({ _id: req.body._id }, req.body);
  res.send(biz);
});

router.post("/addnewcard", tokenAuth, async (req, res) => {
  const { error } = validateBizEdit(req.body);
  if (error) return res.status(400).send(error.details[0].message);
  try {
    let biz = new Biz(
      _.pick(req.body, [
        "profession",
        "address",
        "bizName",
        "description",
        "experience",
        "phone",
        "image",
      ])
    );
    if (!biz.image)
      biz.image =
        "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png";
    biz.email = req.user.email;
    biz.name = req.user.name;

    await biz.save();
    res.send(biz);
  } catch (err) {
    res.send(err);
  }
});

router.delete("/remove/:id", tokenAuth, async (req, res) => {
  let biz = await Biz.findOneAndRemove({
    _id: req.params.id,
  });
  res.send("success");
  if (!biz) res.status(400).send("not-found");
});

module.exports = router;
