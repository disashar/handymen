const express = require("express");
const router = express.Router();
const { User, validate } = require("../models/user");
const { Biz } = require("../models/biz");
const _ = require("lodash");
const bcrypt = require("bcrypt");
const Joi = require("@hapi/joi");
const tokenAuth = require("../middlewares/tokenAuth");
const { valid } = require("@hapi/joi");

router.post("/customersignup", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let user = await User.findOne({ email: req.body.email });
  if (user) return res.status(400).send("Email allready registerd.");

  user = new User(_.pick(req.body, ["name", "email", "password", "biz"]));

  const salt = await bcrypt.genSalt(10);
  user.password = await bcrypt.hash(user.password, salt);
  await user.save();
  res.status(200).send("success!");
});

router.post("/login", async (req, res) => {
  const { error } = validationLogin(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let user = await User.findOne({ email: req.body.email });
  if (!user) return res.status(400).send("Invalid Email");

  const validPass = await bcrypt.compare(req.body.password, user.password);
  if (!validPass) return res.status(400).send("Wrong password");

  res.json({ token: user.generateAuthToken() });
});

router.get("/getFavoritesId", tokenAuth, async (req, res) => {
  let user = await User.findById(req.user._id);
  res.send(user.favorites);
});

router.get("/getFavoritesArr", tokenAuth, async (req, res) => {
  const user = await User.findById(req.user._id);
  const favorites = await Biz.find({ _id: user.favorites });

  res.send(favorites);
});

router.put("/addtofavorites", tokenAuth, async (req, res) => {
  const myFavorite = req.body._id;
  let user = await User.findOneAndUpdate(
    { _id: req.user._id },
    { $push: { favorites: [myFavorite] } }
  );
  res.status(200).send(user);
});

router.put("/removeFromFavorites:id", tokenAuth, async (req, res) => {
  const myFavorite = req.params.id;
  let user = await User.findOneAndUpdate(
    { _id: req.user._id },
    {
      $pull: { favorites: myFavorite },
    }
  );
  res.status(200).send(user);
});

function validationLogin(req) {
  const schemaToCheck = Joi.object({
    email: Joi.string().min(6).max(255).required().email(),
    password: Joi.string().min(6).max(1024).required(),
  });
  return schemaToCheck.validate(req);
}

module.exports = router;
