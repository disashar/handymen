import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import Home from "./components/templates/home";
import Navbar from "./components/templates/navbar";
import UserTemplate from "./components/templates/userTemplate";
import ProtectedUserRoute from "./components/templates/protectedUserRoute";
import ProtectedLoggedRoute from "./components/templates/ProtectedLoggedRoute";
import EditUserTemplate from "./components/templates/editUserTemplate";
import SingleBizCard from "./components/templates/singleBizCard";
import Footer from "./components/templates/footer";
import AddCard from "./components/templates/addCard";
import About from "./components/about";
import CustomerSignup from "./components/customerSignup";
import BizSignup from "./components/bizSignup";
import Login from "./components/login";
import Logout from "./components/logout";
import Favorites from "./components/favorites";
import Proffesional from "./components/proffesional";
import userService from "./services/userService";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: userService.getCurrentUser(),
    };
  }

  render() {
    const { user } = this.state;

    return (
      <React.Fragment>
        <header>
          <Navbar user={user} />
        </header>
        <main style={{ minHeight: 750 }}>
          <Switch>
            <Route path="/" exact render={() => <Home user={user} />} />
            <Route path="/logout" component={Logout} />
            <Route
              path="/proffesionals"
              exact
              render={() => <Proffesional user={user} />}
            />
            <Route path="/about" exact component={About} />
            <Route
              exact
              path="/user/:id"
              render={(props) => <SingleBizCard user={user} {...props} />}
            />

            <ProtectedUserRoute
              exact
              path="/mypage"
              render={(props) => <UserTemplate user={user} {...props} />}
            />
            <ProtectedUserRoute
              exact
              path="/useredit/:id"
              render={(props) => <EditUserTemplate user={user} {...props} />}
            />
            <ProtectedUserRoute
              path="/favorites"
              render={(props) => <Favorites user={user} {...props} />}
            />

            <ProtectedLoggedRoute
              path="/customerSignup"
              component={CustomerSignup}
            />
            <ProtectedLoggedRoute path="/bizSignup" component={BizSignup} />
            <ProtectedLoggedRoute path="/login" component={Login} />
            <ProtectedUserRoute
              exact
              path="/mypage/newcard"
              render={(props) => <AddCard user={user} {...props} />}
            />
          </Switch>
        </main>
        <footer>
          <Footer />
        </footer>
      </React.Fragment>
    );
  }
}

export default App;
