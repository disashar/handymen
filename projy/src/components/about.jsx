import React, { Component } from "react";
import PageHeader from "./templates/pageHeader";

class About extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <div className="container">
          <PageHeader title="About" description='What is "HandyMen" exactly?' />
        </div>
        <div className="col-12 m-4 home-section">
          <p>
            "HandyMen" started as a final project for full stack development
            course and quickly grew into something bigger.
          </p>
          <p>
            Today "HandyMen" is a solution, a community or simply the future.
          </p>
        </div>
      </React.Fragment>
    );
  }
}

export default About;
