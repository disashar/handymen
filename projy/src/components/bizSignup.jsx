import React from "react";
import PageHeader from "./templates/pageHeader";
import Form from "./templates/form";
import Joi from "joi-browser";
import userService from "../services/userService";
import cardService from "../services/cardService";

class BizSignup extends Form {
  state = {
    data: {
      name: "",
      email: "",
      password: "",
      profession: "",
      address: "",
      bizName: "",
      experience: "",
      description: "",
      phone: "",
      image: "",
    },
    errors: {},
  };

  schema = {
    name: Joi.string().required().min(2).label("name"),
    email: Joi.string().required().email().label("email"),
    password: Joi.string().required().min(6).label("password"),
    profession: Joi.string().required().min(2).label("profession"),
    address: Joi.label("address"),
    bizName: Joi.label("bizName"),
    experience: Joi.string().required().min(2).label("experience"),
    description: Joi.string().required().min(2).label("description"),
    phone: Joi.string()
      .min(9)
      .max(15)
      .required()
      .regex(/^0[2-9]\d{7,8}$/),
    image: Joi.string().min(11).uri().allow(""),
  };

  doSubmit = async () => {
    const data = { ...this.state.data };
    if (!data.image) delete data.image;
    data.biz = true;
    try {
      await cardService.bizSignUp(data);
      await userService.login(data.email, data.password);
      window.location = "/mypage";
    } catch (err) {
      if (err.response) {
        this.setState({ errors: { email: err.response.data } });
      }
    }
  };
  handleUpload(event) {
    const { data } = this.state;

    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (e) => {
        data.image = e.target.result;
        this.setState({ data });
      };
    } else {
      alert("please try again");
    }
  }

  render() {
    const { errors, data } = this.state;
    return (
      <div className="container">
        <PageHeader
          title={"Business Account"}
          description={"make sure to fill in the right details"}
        />
        <div className="row">
          <div className="col-lg-6 m-auto">
            <form
              onSubmit={this.handleSubmit}
              className="mt-3"
              autoComplete="off"
            >
              {this.renderInputs("Your Name", "1", "", "Name", "name")}
              {this.renderInputs("email", "2", "", "Email", "email")}
              {this.renderInputs(
                "password",
                "3",
                "password",
                "Password",
                "password"
              )}
              {this.renderInputs(
                "Profession",
                "4",
                "text",
                "developer and etc...",
                "profession"
              )}
              {this.renderInputs(
                "Address",
                "5",
                "text",
                "New York city, 5th avenue...",
                "address",
                true
              )}
              {this.renderInputs(
                `Business name`,
                "6",
                "text",
                "Plumber4U",
                "bizName",
                true
              )}
              {this.renderInputs(
                "Experience",
                "7",
                "text",
                "4 years and 2 months...",
                "experience"
              )}
              {this.renderInputs(
                "Phone Number",
                "8",
                "text",
                "Fill in your phone number",
                "phone"
              )}
              <div className="form-group">
                <label htmlFor="exampleInputEmail9">
                  Upload an Image <span className="text-info">(optional)</span>
                </label>

                <input
                  type="file"
                  className="form-control"
                  id="exampleInputEmail9"
                  name="image"
                  accept="image/png, image/jpeg"
                  onChange={(e) => {
                    this.handleUpload(e);
                  }}
                />

                {data.image && (
                  <React.Fragment>
                    <img src={data.image} alt="userImage" width="100" />
                    <button
                      onClick={() => {
                        delete data.image;
                        this.setState({ data });
                      }}
                    >
                      remove
                    </button>
                  </React.Fragment>
                )}
              </div>
              <label htmlFor="textarea">Description</label>
              <div>
                <textarea
                  className="form-control"
                  onChange={this.handleChange}
                  error={errors["description"]}
                  id="textarea"
                  placeholder="Describe you business in a few words please."
                  name="description"
                ></textarea>
                {errors && (
                  <span className="text-danger">{errors["description"]}</span>
                )}
              </div>
              {this.renderButton("Submit")}
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default BizSignup;
