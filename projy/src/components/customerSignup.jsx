import React from "react";
import PageHeader from "./templates/pageHeader";
import Form from "./templates/form";
import Joi from "joi-browser";
import userService from "../services/userService";

class CustomerSignup extends Form {
  state = {
    data: {
      name: "",
      email: "",
      password: "",
    },
    errors: {},
  };

  schema = {
    name: Joi.string().required().min(2).label("name"),
    email: Joi.string().required().email().label("email"),
    password: Joi.string().required().min(6).label("password"),
  };

  doSubmit = async () => {
    const data = { ...this.state.data };
    data.biz = false;
    try {
      await userService.customerSignUp(data);
      await userService.login(data.email, data.password);
      window.location = "/";
    } catch (err) {
      if (err.response) {
        this.setState({ errors: { email: err.response.data } });
      }
    }
  };

  render() {
    return (
      <div className="container">
        <PageHeader
          title={"Create new user"}
          description={"make sure to fill in the right details!"}
        />
        <div className="row">
          <div className="col-lg-6 m-auto">
            <form
              onSubmit={this.handleSubmit}
              className="mt-3"
              autoComplete="off"
            >
              {this.renderInputs("Your Name", "1", "", "Name", "name")}
              {this.renderInputs("email", "2", "", "Email", "email")}
              {this.renderInputs(
                "password",
                "3",
                "password",
                "Password",
                "password"
              )}
              {this.renderButton("Sign up!")}
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default CustomerSignup;
