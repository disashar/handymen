import React, { Component } from "react";
import PageHeader from "./templates/pageHeader";
import BizCard from "./templates/card";
import favoriteService from "../services/favoriteService";

class Favorites extends Component {
  state = { data: [] };

  async componentDidMount() {
    const { user } = this.props;
    this.setState({ user });
    const { data } = await favoriteService.myFavorites();
    this.setState({ data });
  }

  render() {
    const { user, data } = this.state;

    return (
      <div className="container">
        {user && user.name && (
          <PageHeader
            title={`Hello ${user.name}`}
            description="your favorites proffesionals"
          />
        )}
        <div className="row">
          {data &&
            data.map((card) => (
              <BizCard
                renderName={true}
                description={true}
                key={card._id}
                card={card}
              />
            ))}
          {data && data.length === 0 && <i>no favorites yet...</i>}
        </div>
      </div>
    );
  }
}

export default Favorites;
