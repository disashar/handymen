import React from "react";
import Form from "./templates/form";
import PageHeader from "./templates/pageHeader";
import Joi from "joi-browser";
import userService from "../services/userService";

class Login extends Form {
  state = {
    data: {
      email: "",
      password: "",
    },
    errors: {},
  };

  schema = {
    email: Joi.string().required().email().label("email"),
    password: Joi.string().required().min(6).label("password"),
  };

  doSubmit = async () => {
    const { email, password } = this.state.data;
    try {
      await userService.login(email, password);
      const user = await userService.getCurrentUser();
      if (user.biz) window.location = `/mypage`;
      else window.location = `/favorites`;
    } catch (err) {
      if (err.response && err.response.status === 400) {
        this.setState({ errors: { password: err.response.data } });
      }
    }
  };

  render() {
    return (
      <div className="container">
        <PageHeader title="Login" description="Fill in you Details!" />
        <div className="row">
          <div className="col-lg-6  m-auto">
            <form
              onSubmit={this.handleSubmit}
              className="mt-3"
              autoComplete="off"
            >
              {this.renderInputs(
                "Email",
                "1",
                "",
                "example@gmail.com",
                "email"
              )}
              {this.renderInputs(
                "Password",
                "2",
                "password",
                "example123",
                "password"
              )}

              {this.renderButton("Login!")}
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
