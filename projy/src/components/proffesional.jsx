import React, { Component } from "react";
import PageHeader from "./templates/pageHeader";
import BizCard from "./templates/card";
import cardService from "../services/cardService";
import favoriteService from "../services/favoriteService";

class Proffesional extends Component {
  state = {
    cards: [],
    enable: true,
  };

  async componentDidMount() {
    const { user } = this.props;
    this.setState({ user });
    const { data } = await cardService.getCards();
    if (this.state.user) this.checkFavorites();
    if (data.length > 0) this.setState({ cards: data });
  }

  checkFavorites = async () => {
    const { data } = await favoriteService.checkFavorite();
    this.setState({ userFaveorites: data });
  };

  addToFavorites = async (myFavorite) => {
    await favoriteService.addToFavorites(myFavorite);
    this.checkFavorites();
  };
  removeFromFavorites = async (myFavorite) => {
    await favoriteService.removeFromFavorites(myFavorite);
    this.checkFavorites();
  };

  render() {
    const { cards, user, userFaveorites, enable } = this.state;

    return (
      <div className="container">
        <PageHeader title="Proffesionals" description="business cards" />

        <div className="row">
          {cards.length > 0 &&
            cards.map((card) => (
              <BizCard
                renderName={true}
                description={true}
                enable={enable}
                key={card._id}
                card={card}
                user={user}
                userFaveorites={userFaveorites}
                addToFavorites={() => {
                  this.addToFavorites(card);
                }}
                removeFromFavorites={() => {
                  this.removeFromFavorites(card);
                }}
              />
            ))}
        </div>
      </div>
    );
  }
}

export default Proffesional;
