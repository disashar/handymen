import React from "react";
import { Route, Redirect } from "react-router-dom";
import userService from "../../services/userService";
const ProtectedLoggedRoute = ({
  path,
  component: Component,
  render,
  ...rest
}) => {
  const user = userService.getCurrentUser();

  return (
    <Route
      exact
      {...rest}
      render={(props) => {
        if (user)
          return (
            <Redirect
              to={{
                pathname: "/",
                state: { from: props.location },
              }}
            />
          );
        return <Component {...props} />;
      }}
    />
  );
};

export default ProtectedLoggedRoute;
