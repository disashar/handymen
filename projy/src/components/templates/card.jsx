import React from "react";
import { Link } from "react-router-dom";

const BizCard = ({
  renderName,
  card,
  user,
  addToFavorites,
  removeFromFavorites,
  userFaveorites,
  enable,
  edit,
  description,
  remove,
  deleteCard,
}) => {
  return (
    <React.Fragment>
      <div className="col-md-6 col-lg-4 mt-3">
        <div className="card">
          <p className="card-text">
            {edit && (
              <Link
                to={`/useredit/${card._id}`}
                className="fa fa-edit ml-1 align-middle"
                style={{ fontSize: "20px" }}
              />
            )}
            {remove && (
              <Link
                className="ml-3 fa fa-trash align-middle"
                to={""}
                style={{ fontSize: "20px" }}
                onClick={(e) => deleteCard(e, card._id)}
              />
            )}
          </p>
          <img
            src={card.image}
            className="card-img-top"
            alt={`${card.name}`}
            height="250px"
            width="250px"
          />
          {renderName && (
            <h1 style={{ textDecoration: "underline" }} className="ml-5">
              <Link to={`/user/${card._id}`}>{card.name}</Link>
            </h1>
          )}
          <div className="card-body">
            <h5 className="card-title">
              {card.profession} {card.bizName && `-${card.bizName}`}
            </h5>
            {description && <p className="card-text">{card.description}</p>}
            <p className="card-text border-top pt-2">
              <b>
                <span className="text-success">{card.experience}</span> of
                experience{" "}
              </b>
              <br />
              <b> {card.address && `from: ${card.address}`}</b>
              <br />
              <a href={`mailto: ${card.email}`}> {card.email}</a>
            </p>
            {enable &&
              user &&
              userFaveorites &&
              !userFaveorites.includes(card._id) && (
                <button onClick={addToFavorites} className="btn btn-info">
                  Add to Favorites
                </button>
              )}
            {enable &&
              user &&
              userFaveorites &&
              userFaveorites.includes(card._id) && (
                <button
                  onClick={removeFromFavorites}
                  className="btn btn-danger"
                >
                  Remove from Favorites
                </button>
              )}
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default BizCard;
