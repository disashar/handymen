import React from "react";
import PageHeader from "./pageHeader";
import cardService from "../../services/cardService";
import Form from "./form";
import Joi from "joi-browser";
import { Redirect, Link } from "react-router-dom";

class EditUserTemplate extends Form {
  state = {
    data: {},
    errors: {},
  };

  schema = {
    _id: Joi.string(),
    profession: Joi.string().required().min(2).label("profession"),
    address: Joi.label("address"),
    bizName: Joi.label("bizName"),
    experience: Joi.string().required().min(2).label("experience"),
    description: Joi.string().required().min(2).label("description"),
    phone: Joi.string()
      .min(9)
      .max(15)
      .required()
      .regex(/^0[2-9]\d{7,8}$/),
    image: Joi.string().min(11).uri().allow(""),
  };

  getCardsDetails = async () => {
    const { data } = await cardService.getCardByCardId(
      this.props.match.params.id
    );
    delete data.__v;
    delete data.name;
    delete data.email;
    return data;
  };

  componentDidMount = async () => {
    const { user } = this.props;
    this.setState({ user });
    const data = await this.getCardsDetails();
    this.setState({ data });
  };

  doSubmit = async () => {
    const { data } = this.state;
    await cardService.editCard(data);
    this.props.history.replace(`/mypage`);
  };

  componentWillUnmount() {
    // fix Warning: Can't perform a React state update on an unmounted component
    this.setState = () => {
      return;
    };
  }
  handleUpload(event) {
    const { data } = this.state;

    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (e) => {
        data.image = e.target.result;
        this.setState({ data });
      };
    } else {
      alert("please try again");
    }
  }
  render() {
    const { data, errors, user } = this.state;
    return (
      <div className="container">
        {user && !user.biz && (
          <Redirect
            to={{
              pathname: "/favorites",
              state: { from: this.props.location },
            }}
          />
        )}
        {data && data.profession && (
          <React.Fragment>
            <PageHeader
              title={user.name}
              description={"please update your details "}
            />
            <div className="row">
              <div className="col-lg-6 m-auto">
                <form
                  onSubmit={this.handleSubmit}
                  className="mt-3"
                  autoComplete="off"
                >
                  {this.renderInputs(
                    "Profession",
                    "4",
                    "text",
                    "developer and etc...",
                    "profession"
                  )}
                  {this.renderInputs(
                    "Address (optional)",
                    "5",
                    "text",
                    "New York city, 5th avenue...",
                    "address"
                  )}
                  {this.renderInputs(
                    "Business Name (optional)",
                    "6",
                    "text",
                    "Plumber4U",
                    "bizName"
                  )}
                  {this.renderInputs(
                    "Experience",
                    "7",
                    "text",
                    "4 years and 2 months...",
                    "experience"
                  )}
                  {this.renderInputs(
                    "Phone Number",
                    "8",
                    "text",
                    "Fill in your phone number",
                    "phone"
                  )}
                  <div className="form-group">
                    <label htmlFor="exampleInputEmail9">upload image</label>
                    <input
                      accept=""
                      type="file"
                      className="form-control"
                      id="exampleInputEmail9"
                      name="image"
                      onChange={(e) => {
                        this.handleUpload(e);
                      }}
                    />

                    <img src={data.image} alt="imageTest" width="100" />
                    <button
                      onClick={() => {
                        delete data.image;
                        this.setState({ data });
                      }}
                    >
                      remove
                    </button>
                  </div>
                  <br />
                  <label htmlFor="textarea">Description</label>
                  <div>
                    <textarea
                      className="form-control"
                      onChange={this.handleChange}
                      error={errors["description"]}
                      id="textarea"
                      placeholder="Describe you business in a few words please."
                      name="description"
                      value={data.description}
                    ></textarea>
                    {errors && (
                      <span className="text-danger">
                        {errors["description"]}
                      </span>
                    )}
                  </div>
                  {this.renderButton("Submit")}
                  <Link
                    to={"/mypage"}
                    className="btn btn-outline-secondary float-right"
                  >
                    Back
                  </Link>
                </form>
              </div>
            </div>
          </React.Fragment>
        )}
      </div>
    );
  }
}

export default EditUserTemplate;
