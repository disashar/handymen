import React from "react";

const Footer = () => {
  const date = new Date();
  return (
    <footer className="mt-3 page-footer font-small blue bg-success ">
      <div className="footer-copyright text-center py-3">
        © {date.getFullYear()} Copyright:
        <a
          id="copyRightLink"
          className="text-white pl-2"
          href={"mailto:disashar@gmail.com"}
        >
          Disashar
        </a>
      </div>
    </footer>
  );
};

export default Footer;
