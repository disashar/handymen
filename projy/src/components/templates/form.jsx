import React, { Component } from "react";
import Input from "./input";
import Joi from "joi-browser";

class Form extends Component {
  state = {
    data: {},
    errors: {},
  };

  validate() {
    const options = { abortEarly: false };

    const { error } = Joi.validate(this.state.data, this.schema, options);
    if (!error) return null;
    const errors = {};
    for (let item of error.details) {
      errors[item.path[0]] = item.message;
    }

    return error;
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const errors = this.validate();
    this.setState({ errors: errors || {} });
    if (errors) return;
    this.doSubmit();
  };

  validateProperty({ name, value }) {
    const { error } = Joi.validate(
      { [name]: value },
      { [name]: this.schema[name] }
    );
    return error ? error.details[0].message : null;
  }

  handleChange = ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const errorMsg = this.validateProperty(input);
    if (errorMsg) errors[input.name] = errorMsg;
    else delete errors[input.name];
    const data = { ...this.state.data };
    data[input.name] = input.value;
    this.setState({ data, errors });
  };

  renderInputs(label, forId, type = "text", holder, name, optional) {
    const { data, errors } = this.state;
    return (
      <React.Fragment>
        <Input
          onChange={this.handleChange}
          error={errors[name]}
          type={type}
          forId={forId}
          label={label}
          holder={holder}
          name={name}
          value={data[name] || ""}
          optional={optional}
        />
      </React.Fragment>
    );
  }
  renderButton(Name) {
    return (
      <button
        disabled={this.validate()}
        type="submit"
        className="btn btn-primary"
      >
        {Name}
      </button>
    );
  }
}

export default Form;
