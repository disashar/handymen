import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../../css/home.css";
class Home extends Component {
  state = {};
  render() {
    return (
      <div className="row">
        <div className="col-12 text-center mt-4">
          <h1 className="display-4">
            HandiMen<i className="fa fa-handshake-o ml-2"></i>
          </h1>
          <h4>The freelancer's choice.</h4>
        </div>
        <div className="col-12 m-4 home-section">
          Using handimen can solve all your needs. youre just one step away from
          becoming part of the biggest freelancers community
        </div>
        {!this.props.user && (
          <React.Fragment>
            <div className="col-12 text-center">
              <h3>Create account</h3>
              <Link to="/customerSignup" className="btn btn-success m-4">
                Customer
              </Link>
              <Link to="/bizSignup" className="btn btn-success m-4">
                Business
              </Link>
            </div>
            <div className="col-12 text-center">
              <h3>Already registered?</h3>
              <Link to="/login" className="btn btn-primary m-4">
                Log-In
              </Link>
            </div>
          </React.Fragment>
        )}
      </div>
    );
  }
}

export default Home;
