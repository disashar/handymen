import React from "react";

const Input = ({ label, holder, forId, error, optional, ...rest }) => {
  return (
    <div className="form-group">
      <label htmlFor={`exampleInputEmail${forId}`}>
        {label}
        {optional && <span className="text-info"> (optional)</span>}
      </label>
      <input
        {...rest}
        className="form-control"
        id={`exampleInputEmail${forId}`}
        placeholder={holder}
      />
      {error && <span className="text-danger">{error}</span>}
    </div>
  );
};

export default Input;
