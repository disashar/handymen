import React, { Component } from "react";
import { Link, NavLink } from "react-router-dom";

class Navbar extends Component {
  state = {};
  render() {
    const { user } = this.props;

    return (
      <nav id="mainNav" className="navbar navbar-expand-lg shadow-lg">
        <div className="container-fluid">
          <Link className="navbar-brand pr-5 " to="/">
            <i id="brand" className="fa fa-handshake-o ml-2"></i>
          </Link>

          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <i className="fa fa-bars"></i>
          </button>
          <div
            className="collapse navbar-collapse pl-5"
            id="navbarNavAltMarkup"
          >
            <div className="navbar-nav ml-auto">
              <NavLink className="nav-link pr-5 " to="/about">
                About
              </NavLink>
              <NavLink className="nav-link pr-5 " to="/proffesionals">
                Proffesionals
              </NavLink>
              {user && (
                <NavLink
                  activeClassName="active"
                  className="nav-link pr-5"
                  to="/favorites"
                >
                  My Favorites
                </NavLink>
              )}
              {!user && (
                <React.Fragment>
                  <div className="dropdown">
                    <div
                      id="dropdownMenuButton"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                      className="btn btn-light dropdown-toggle"
                    >
                      Sign Up
                    </div>
                    <div
                      style={{ minWidth: "6rem" }}
                      aria-labelledby="dropdownMenuButton"
                      className="dropdown-menu text-center"
                    >
                      <NavLink className="nav-link" to="/customerSignup">
                        Customer
                      </NavLink>
                      <NavLink className="nav-link" to="/bizSignup">
                        Business
                      </NavLink>
                    </div>
                  </div>
                  <NavLink className="nav-link pl-5" to="/login">
                    Log In
                  </NavLink>
                </React.Fragment>
              )}
              {user && (
                <React.Fragment>
                  <NavLink
                    id="myPage"
                    className="nav-link btn btn-success bg-light text-success  mx-3"
                    to={`/mypage`}
                  >
                    {user.name}
                  </NavLink>
                  <NavLink
                    className="nav-link btn btn-danger bg-light text-danger "
                    to="/logout"
                  >
                    Logout
                  </NavLink>
                </React.Fragment>
              )}
            </div>
          </div>
        </div>
      </nav>
    );
  }
}

export default Navbar;
