import React, { Component } from "react";
import BizCard from "./card";
import PageHeader from "./pageHeader";
import cardService from "../../services/cardService";
import favoriteService from "../../services/favoriteService";
import { Redirect } from "react-router-dom";

class SingleBizCard extends Component {
  state = {};

  componentDidMount = async () => {
    const { user } = this.props;
    const { data } = await cardService.getCardByCardId(
      this.props.match.params.id
    );
    this.setState({ data, user });
    if (this.state.user) this.checkFavorites();
  };

  async checkFavorites() {
    const { data } = await favoriteService.checkFavorite();
    this.setState({ userFaveorites: data });
  }

  addToFavorites = async (myFavorite) => {
    await favoriteService.addToFavorites(myFavorite);
    this.checkFavorites();
  };
  removeFromFavorites = async (myFavorite) => {
    await favoriteService.removeFromFavorites(myFavorite);
    this.checkFavorites();
  };

  render() {
    const { userFaveorites, user, data } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col-lg-12 mx-auto">
              {data && (
                <PageHeader title={data.name} description={data.description} />
              )}
              {data === "sorry not found" && (
                <Redirect
                  to={{
                    pathname: "/",
                    state: { from: this.props.location },
                  }}
                />
              )}
              {data && data.name && (
                <BizCard
                  card={data}
                  user={user}
                  enable={true}
                  userFaveorites={userFaveorites}
                  addToFavorites={() => {
                    this.addToFavorites(data);
                  }}
                  removeFromFavorites={() => {
                    this.removeFromFavorites(data);
                  }}
                />
              )}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default SingleBizCard;
