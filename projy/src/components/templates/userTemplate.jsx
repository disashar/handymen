import React, { Component } from "react";
import PageHeader from "./pageHeader";
import cardService from "../../services/cardService";
import BizCard from "./card";
import { Redirect, Link } from "react-router-dom";
import Swal from "sweetalert2";

class UserTemplate extends Component {
  state = {
    data: {},
  };

  componentDidMount = async () => {
    const { user } = this.props;
    this.setState({ user });
    await this.getCardsDetails();
  };

  getCardsDetails = async () => {
    const { data } = await cardService.getCardsByUserId();
    if (data !== this.state.data) this.setState({ data });
  };

  componentWillUnmount() {
    this.setState = () => {
      return;
    };
  }

  handleTitle() {
    const { user, data } = this.state;
    return data.bizName ? `${user.name} ${data.bizName}` : user.name;
  }

  handleDelete = (e, cardId) => {
    e.preventDefault();
    Swal.fire({
      type: "warning",
      title: "Are you Sure?",
      showCancelButton: true,
      confirmButtonColor: "#d9534f",
      cancelButtonColor: "#3085d6",
      confirmButtonText: "Delete !",
    }).then(async (result) => {
      if (result.value) {
        this.deleteCard(cardId);
      }
    });
  };

  deleteCard = async (cardId) => {
    await cardService.removeCard(cardId);
    this.getCardsDetails();
  };

  render() {
    const { data, user } = this.state;
    return (
      <div className="container">
        {user && !user.biz && (
          <Redirect
            to={{
              pathname: "/favorites",
              state: { from: this.props.location },
            }}
          />
        )}

        {user && (
          <React.Fragment>
            <PageHeader
              title={this.handleTitle()}
              description={"Your Own Page"}
            />
            <Link to="/mypage/newcard" className="ml-3 btn btn-info">
              Add a Business Card<i className="ml-4 fa fa-address-card"></i>
            </Link>
            <div className="row">
              {data &&
                data.length > 0 &&
                data.map((card) => (
                  <BizCard
                    remove={true}
                    description={true}
                    edit={true}
                    card={card}
                    enable={true}
                    key={card._id}
                    user={user}
                    deleteCard={this.handleDelete}
                  />
                ))}
            </div>
          </React.Fragment>
        )}
      </div>
    );
  }
}

export default UserTemplate;
