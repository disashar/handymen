import http from "./httpService";
const apiUrl = `http://localhost:8000`;

export const getCardsByUserId = () => {
  return http.get(`${apiUrl}/bizs/getcardbyuserid`);
};
export const getCardByCardId = (cardId) => {
  return http.get(`${apiUrl}/bizs/getcardbycardid`, { params: { id: cardId } });
};

export const getCards = () => {
  return http.get(`${apiUrl}/bizs`);
};

export const editCard = (card) => {
  return http.put(`${apiUrl}/bizs`, card);
};

export const bizSignUp = (data) => {
  return http.post(`${apiUrl}/bizs/bizsignup`, data);
};
export const addCard = (card) => {
  return http.post(`${apiUrl}/bizs/addnewcard`, card);
};
export const removeCard = (cardId) => {
  return http.delete(`${apiUrl}/bizs/remove/${cardId}`);
};

export default {
  bizSignUp,
  getCards,
  editCard,
  getCardsByUserId,
  getCardByCardId,
  addCard,
  removeCard,
};
