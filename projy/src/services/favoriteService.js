import http from "./httpService";
const apiUrl = `http://localhost:8000`;

export function checkFavorite() {
  return http.get(`${apiUrl}/users/getFavoritesId`);
}

export function myFavorites() {
  return http.get(`${apiUrl}/users/getFavoritesArr`);
}

export function addToFavorites(myFavorite) {
  return http.put(`${apiUrl}/users/addtofavorites`, myFavorite);
}

export function removeFromFavorites(myFavorite) {
  return http.put(`${apiUrl}/users/removeFromFavorites${myFavorite._id}`);
}

export default {
  checkFavorite,
  myFavorites,
  addToFavorites,
  removeFromFavorites,
};
