import http from "./httpService";
import JwtDecode from "jwt-decode";
const apiUrl = `http://localhost:8000`;

const tokenKey = "token";

export function customerSignUp(data) {
  return http.post(`${apiUrl}/users/customersignup`, data);
}

export function logout() {
  localStorage.removeItem(tokenKey);
}

export function getCurrentUser() {
  try {
    const jwt = localStorage.getItem(tokenKey);
    return JwtDecode(jwt);
  } catch (err) {
    return null;
  }
}

export async function login(email, password) {
  const { data } = await http.post(`${apiUrl}/users/login`, {
    email,
    password,
  });
  localStorage.setItem(tokenKey, data.token);
}

export function getJwt() {
  return localStorage.getItem(tokenKey);
}
export default { login, getCurrentUser, logout, getJwt, customerSignUp };
